const { Router } = require('express');
const router = Router();

const { uploadImage } = require('../controller/upload.controller'); 
const { validateToke, validateRole } = require('../middleware/authentication')

router.put('/upload/:type/:id', uploadImage)


module.exports = router;