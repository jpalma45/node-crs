const { Router } = require('express');
const app = Router();

const { Login, google } = require('../controller/login.controller');

app.post('/login', Login);
app.post('/google', google);

module.exports = app;