const argv = require('./config/yargs').argv;
const colors = require('colors');
const porHacer = require('./TO-DO/to-do');

let comando = argv._[0];

switch ( comando ) {
    case 'crear':
        let task = porHacer.crear(argv.description);
        console.log(task);      
        break;

    case 'listar':
        let listado = porHacer.getListado();     
        
        for(let task of listado){

            console.log('====Por hacer==='.green);
            console.log(task.description);
            console.log('Estado: ', task.completado);
            console.log('==============='.green);

        }
        break;

    case 'actualizar':
        let updatedData = porHacer.actualizar(argv.description, argv.completado);
        console.log(updatedData);
        break;
    
    case 'delete':
        let dataDeleted = porHacer.deleted(argv.description);
        console.log(dataDeleted);
        break;

    default:
        console.log('comando no reconocido')
        break;
}