const bcrypt = require('bcrypt');
const _ = require('underscore');
const User = require('../models/user.model');

const GetUsers = async ( req, res, next ) => {
    let initPage = req.query.initPage;
    initPage = Number(initPage);

    let limitPage = req.query.limitPage;
    limitPage = Number(limitPage);

    await User.find({ status: true }, 'name email role status  google img')
    .skip(initPage)
    .limit(limitPage)
    .exec((err, dataUser) => {
        if (err) {
            res.status(500).json({
                message: 'Error get users',
                userStorage: {}
            });

            next(err);

            throw Error('Users no get', err);
        } else {
            User.count({ status: true }, (err, countUsers) => {
                res.status(200).json({
                    message: 'Get users',
                    dataUser,
                    countUsers
                });
            });
        }
    });
}

const GetUser = async ( req, res, next ) => {
    const { id } = req.params

    await User.findById(id, (err, dataUser) => {
        if (err) {
            res.status(500).json({
                message: 'Error get user',
                userStorage: {}
            });

            next(err);

            throw Error('User no get', err);
        } else {
            res.status(200).json({
                message: 'Get user',
                dataUser
            });
        }
    });
}

const saveUsers = async ( req, res, next ) => {
    const data = req.body;
    data.password = bcrypt.hashSync(data.password, 10);
    const NewUser = new User(data);

    await NewUser.save((err, userStorage) => {

        if (err) {
            res.status(500).json({
                message: 'Error save user',
                err, 
                userStorage: {}
            });

            next(err);
        } else {

            res.status(200).json({
                message: 'Save user',
                userStorage
            });

            next();
        }

    });
    
}

const deleteUsers = ( req, res, next ) => {
    const { id } = req.params

    const status = {
        status: false
    };

    User.findByIdAndUpdate(id, status, { new: true } , (err, userUpdated) => {
        if(err) {
        res.status(500).json({
            message: 'Error in update user status',
            err, 
            userStorage: {}
        });

        next(err);
    } else {

        res.status(200).json({
            message: 'Updated user status',
            userUpdated
        });

        next();
    }
    });

}

const updateUsers = ( req, res, next ) => {
    const { id } = req.params
    const dataUpdated = _.pick(req.body, [ 'name', 'email', 'img', 'role', 'status' ]);

    User.findByIdAndUpdate(id, dataUpdated, { new: true, runValidators: true } , (err, userUpdated) => {
        if(err) {
        res.status(500).json({
            message: 'Error in update user',
            err, 
            userStorage: {}
        });

        next(err);
    } else {

        res.status(200).json({
            message: 'Updated user',
            userUpdated
        });

        next();
    }
    });
}



module.exports = {
    GetUsers,
    GetUser,
    saveUsers,
    deleteUsers,
    updateUsers
}