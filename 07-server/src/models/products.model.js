const { model, Schema } = require('mongoose');

const ProductSchema = new Schema({
    name: { type: String, required: [true, 'the name of the product is required'] },
    uniPrice: { type: Number, required: [true, 'the uniPrice of the product is required'] },
    description: { type: String, required: false },
    img: { type: String, required: false },
    available: { type: Boolean, required: true, default: true },
    category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
    user: { type: Schema.Types.ObjectId, ref: 'User' }
});

module.exports = model('Product', ProductSchema);