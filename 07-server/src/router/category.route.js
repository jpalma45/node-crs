const { Router } = require('express');
const app = Router();

const { validateRole, validateToke } = require('../middleware/authentication');

const { saveCategory, getCategories, getCategory,
        updateCategory, deleteCategory } = require('../controller/category.controller');

app.post('/category', [validateToke, validateRole], saveCategory);
app.get('/categories', validateToke, getCategories);
app.get('/category/:id', validateToke, getCategory);
app.put('/category/:id', [validateToke, validateRole], updateCategory);
app.delete('/category/:id', [validateToke, validateRole], deleteCategory);

module.exports = app;