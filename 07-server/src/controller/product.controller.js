const _ = require('underscore');
const { Response } = require('../response/Response.response');
const Product = require('../models/products.model');

const getProducts = async (req, res, next) => {
    let { from } = req.query || 0;
    from = Number(from);

    await Product.find({available: true})
                 .skip(from)
                 .limit(5)
                 .populate('user', 'name email')
                 .populate('category', 'description')
                 .exec((err, dataProducts) =>{
                    Response(res, err, dataProducts, next);
                });
}

const getProduct = async (req, res, next) => {
    const { id } = req.params;

    await Product.findById(id) 
                .populate('user', 'name email')
                .populate('category', 'description')
                .exec((err, dataProduct) => {
                            Response(res, err, dataProduct, next);
                        });
}

const saveProduct = async (req, res, next) => {
    const dataProduct = req.body;
    const newProduct = new Product(dataProduct);
    newProduct.user = req.user._id;

    await newProduct.save((err, dataSave) => {
        Response(res, err, dataSave, next);
    });
}

const getPerTermQ = (req, res, next) => {
    const { term } = req.params;

    let regex = new RegExp(term, 'i');

    Product.find({name: regex})
            .populate('category', 'name')
            .exec((err, productsDB) => {
                Response(res, err, productsDB, next);
            });
}

const updateProduct = async (req, res, next) => {
    const { id } = req.params;
    const dataUpdate = _.pick(req.body, ['name', 'uniPrice', 'category', 'available', 'description']);

    await Product.findById(id, (err, productDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'The Id done no exit'
                }
            });
        }

        productDB.name = dataUpdate.name;
        productDB.uniPrice = dataUpdate.uniPrice;
        productDB.category = dataUpdate.category;
        productDB.available = dataUpdate.available;
        productDB.description = dataUpdate.description;

        productDB.save((err, productSave) => {
            Response(res, err, productSave, next);
        });
    });

}

const deleteProduct = async (req, res, next) => {
    const { id } = req.params;
    await Product.findById(id, (err, productDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'The Id done no exit'
                }
            });
        }

        productDB.available = false;

        productDB.save((err,  productSave) => {
            Response(res, err, productSave, next);
        });
    });
}

module.exports = {
    getProducts,
    getProduct,
    saveProduct,
    updateProduct,
    deleteProduct,
    getPerTermQ
};