const axios = require('axios').default;

const getLugarLatLng = async ( direccion ) => {
    
    const encodeUrl = encodeURI( direccion );

    const instance = axios.create({
        baseURL: 'https://community-open-weather-map.p.rapidapi.com',
        headers: {'x-rapidapi-key': '7a150ebbd0msh1f99d36c866ee7dp1c6146jsnac434b6cbe22'}
    });

    const response = await instance.get(`https://community-open-weather-map.p.rapidapi.com/find?q=${encodeUrl}`);

    if( response.data.list.length === 0 ) {
        throw new Error(`No hay resultado para: ${direccion}`)
    }

    const data = response.data.list[0];
    const { name, coord: { lat, lon } } = data;


    return {
        name,
        lat,
        lon
    }
}

module.exports = {
    getLugarLatLng
}