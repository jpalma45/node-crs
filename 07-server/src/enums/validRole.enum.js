let ValidRole = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} is no a role valid'
};

module.exports = {
    ValidRole
};