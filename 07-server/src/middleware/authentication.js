const jwt = require('jsonwebtoken');

let validateToke = ( req, res, next ) => {
    let token = req.get('Authorization');

    jwt.verify(token, process.env.seed_token, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: 'Invalid token'
            });
        }

        req.user = decoded.user;
        next();
    });
};

let validateRole = ( req, res, next ) => {
  let user = req.user;

  if (user.role === 'ADMIN_ROLE') {
      next();
  } else {
      return res.json({
          ok: false,
          err: {
            message: 'the no have role admin'
          }
      });
  }
};

/**
 * @owner Johan palma
 * @summary [2020-08-31] Validate token by img
 */

 const validateTokenImg = (req, res, next) => {
     
    let token = req.query.token; 

    jwt.verify(token, process.env.seed_token, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: 'Invalid token'
            });
        }

        req.user = decoded.user;
        next();
    });
 }

module.exports = {
    validateToke,
    validateRole,
    validateTokenImg
}