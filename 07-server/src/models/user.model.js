const { Schema, model } = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const { ValidRole } = require('../enums/validRole.enum');

const UserSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is required']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'The email is required']
    },
    password: {
        type: String,
        required: [true, 'The password is required']
    },
    img:{
        type: String, 
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: ValidRole
    },
    status: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    }
});

UserSchema.methods.toJSON = function() {

    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
}

UserSchema.plugin(uniqueValidator, { message: '{PATH} is unique' });

module.exports = model('User',UserSchema);