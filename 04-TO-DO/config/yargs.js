const description = {
    demand: true,
    alias: 'd'
}

const completado = {
    alias: 'c',
    default: true
}

const optsCrear = {
    description
}
const optsActualizar = {
    description,
    completado
}

const optsDelete = {
    description
}

const argv = require('yargs')
.command('crear', 'Crear un elemento por hacer', optsCrear)
.command('actualizar', 'Actualiza el estado completado de una tarea', optsActualizar)
.command('delete', 'Elimina una tarea de la lista', optsDelete)
.help()
.argv;

module.exports = {
    argv
}