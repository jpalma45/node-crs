const argv = require('./config/yargs').argv;
const colors = require('colors');

const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');

const getInfo = async (direccion) => {

    try {
        const respLugar = await lugar.getLugarLatLng(direccion);
   
        const respClima = await clima.getClima( respLugar.lat, respLugar.lon );
     
        const { temp } = respClima;

        if(Object.keys(respClima).length === 0) {
            return `No se pudo determinar el clima de: ${direccion}`.red;
        } else {
            return `El clima de ${direccion} es de ${temp}`.green;
        }

    } catch (error) {
        throw new Error(`No se pudo determinar el clima de: ${direccion}`.red, error)
    }

}

getInfo(argv.direccion)
  .then(resp => console.log(resp))
  .catch(err => console.log(err));