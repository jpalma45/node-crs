// PORT
process.env.PORT = process.env.PORT || 4000;

// Environment
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// date expired token
// expired in one hour
process.env.expired_token = 60 * 60 * 24 * 30;

// seed by authentication
process.env.seed_token =  process.env.seed_token || 'secret-palma';

// DataBase

let urlDATABASE;

if (process.env.NODE_ENV === 'dev') {
    urlDATABASE = 'mongodb://localhost:27017/db07';
} else {
    // create environmental variables in heroku for cmd: heroku config:set MONGO_URI='VALUE'
    // get environmental variables in heroku for cmd: heroku config:get MONGO_URI
    // delete environmental variables in heroku for cmd: heroku config:unset MONGO_URI
    urlDATABASE = 'mongodb+srv://dbUser:dbUser@cluster0.3dxyb.mongodb.net/07ServerDB?retryWrites=true&w=majority';
}

process.env.URLDB = urlDATABASE;

// Google client ID
// esto viene de: https://console.developers.google.com/apis/credentials 
// para obtener el ID se debe crear una Outh client id - aplication web 
// en los detalles se puede ver el ID

process.env.CLIENT_ID = process.env.CLIENT_ID || '203654451371-sorf81rvosi6q599jo10d3kmv96hu9eo.apps.googleusercontent.com';