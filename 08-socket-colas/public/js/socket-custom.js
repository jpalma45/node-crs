var socket = io();

socket.on("connect", () => {
  console.log("Connected to server");
});

socket.on("disconnect", () => {
  console.log("socket is not connected to the server");
});

socket.emit(
  "data",
  {
    User: "Johan",
    Mesaje: "HELLO WORD",
  },
  (response) => {
    console.log("Server response: ", response);
  }
);

socket.on("data", (data) => {
  console.log(data);
});
