const axios = require('axios').default;

const getClima = async (lat, lon) => {

    const instance = axios.create({
        baseURL: 'https://community-open-weather-map.p.rapidapi.com',
        headers: {'x-rapidapi-key': '7a150ebbd0msh1f99d36c866ee7dp1c6146jsnac434b6cbe22'}
    });

    const response = await instance.get(`https://community-open-weather-map.p.rapidapi.com/weather?lat=${lat}&lon=${lon}&units=metric`)

    const { main: { temp } } = response.data;

    return {
        temp
    }
}

module.exports = {
    getClima
}