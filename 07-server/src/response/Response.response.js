const Response = (res, err, data, next) => {
    if (err) {
        res.status(500).json({
            ok: false,
            err
        });
    }

    if (!data) {
        res.status(400).json({
            ok: false,
            err
        });
        next(err);
    }

    res.json({
        ok: true,
        data
    });
}

module.exports = {
    Response
}