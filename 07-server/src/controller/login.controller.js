const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

const User = require('../models/user.model');

const Login = async (req, res, next) => {
  const { email, password } = req.body;

  await User.findOne({ email }, (err, dataUser) => {
      if(err) return res.status(500).json({ message: 'Error in get data user' }); next(err)

      if(!dataUser) return res.status(400).json({message: 'user no find', data: {}}); next()

      if(!bcrypt.compareSync( password, dataUser.password )) {

        return res.status(400).json({message: 'incorrect password', data: {}});

      }

      let token = jwt.sign({
          user: dataUser
      }, process.env.seed_token, { expiresIn: process.env.expired_token });

      res.json({
          ok: true,
          message: 'data user',
          dataUser,
          token
      });
  });
}

// configuraciones de google
async function verify( token ) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const payload = ticket.getPayload();
  return {
    name: payload.name,
    email: payload.email,
    img: payload.picture,
    google: true
  };

}

const google = async (req, res, next) => {
  const { token } = req.body; 

  let googleUser = await verify(token)
  .catch(err => {
    return res.status(403).json({
      ok: false,
      error: err
    });
  });

  User.findOne({ email: googleUser.email }, (err, userDB) => {
    if(err) return res.status(500).json({ ok: false, err }); next(err)

    if (userDB) {

      if (userDB.google === false) {
        if(err) return res.status(500).json({ ok: false, err:{  message: 'use normal authentication' } });
      } else {
        let token = jwt.sign({
          user: userDB
        }, process.env.seed_token, { expiresIn: process.env.expired_token });

        return res.json({
          ok: true,
          user: userDB,
          token
        });
      }

    } else {
      // if no exit user in the database
      let user = new User(googleUser);
      user.password = ':)';

      user.save((err, userDB) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            err
          });
        }

        let token = jwt.sign({
          user: userDB
        }, process.env.seed_token, { expiresIn: process.env.expired_token });

        return res.json({
          ok: true,
          user: userDB,
          token
        });
      })
    }

  });
}

module.exports = {
    Login,
    google
}