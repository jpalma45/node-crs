const express = require('express');
const app = express();

app.use(require('./login.route'));
app.use(require('./user.router'));
app.use(require('./category.route'));
app.use(require('./product.route'));
app.use(require('./upload.router'));
app.use(require('./images.route'));

module.exports = app;