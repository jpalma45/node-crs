const _ = require('underscore');
const { Response } = require('../response/Response.response');
const Category = require('../models/category.model');

const getCategories = (req, res, next) => {
    Category.find({}).sort('description').populate('user', 'name email').exec((err, Categories) => {
        Response(res, err, Categories, next);
    });
}

const getCategory = async (req, res, next) => {
    const { id } = req.params;

    await Category.findById(id).populate('user', 'name email').exec((err, category) => {
        Response(res, err, category, next);
    });
}

const saveCategory = async (req, res, next) => {
    const category = req.body;
    const NewCategory = new Category(category);

    await NewCategory.save((err, storedCategory) => {
        Response(res, err, storedCategory, next);
    })
}

const updateCategory = async (req, res, next) => {
    const { id } = req.params;
    const dataUpdate = _.pick(req.body, ['description']);

    await Category.findByIdAndUpdate(id, dataUpdate, { new: true, runValidators: true }, (err, categoryUpdate) => {
        Response(res, err, categoryUpdate, next);
    })
}

const deleteCategory = async (req, res, next) => {
    const { id } = req.params;

    await Category.findByIdAndRemove(id, (err, dataDelete) => {
        Response(res, err, dataDelete, next);
    });
}

module.exports = {
    getCategories,
    getCategory,
    saveCategory,
    updateCategory,
    deleteCategory
}