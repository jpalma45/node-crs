const { Schema, model } = require('mongoose');

const CategorySchema = new Schema({
    description: { type: String, unique: true, required: [true, 'the category description is required'] },
    user: { type: Schema.Types.ObjectId, ref: 'User' }
});

module.exports = model('Category', CategorySchema);