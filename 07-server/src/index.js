const app = require('./app');
require('./config/config');
const mongoose = require('mongoose');

const main = async () => {
    mongoose.connect(process.env.URLDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    }).then(() => {
        app.listen(process.env.PORT, () => {
            console.log(`Server on port: ${process.env.PORT}`);
        });
    }).catch(err => console.error(`database no connected: ${err}`));

};

main();