const { Router } = require('express');
const router = Router();

const { GetUsers, GetUser, deleteUsers, saveUsers, updateUsers } = require('../controller/user.controller');
const { validateToke, validateRole } = require('../middleware/authentication')

router.post('/user', [validateToke, validateRole], saveUsers);
router.get('/user', validateToke, GetUsers);
router.get('/user/:id',validateToke, GetUser);
router.put('/user/:id', [validateToke, validateRole], updateUsers);
router.delete('/user/:id', [validateToke, validateRole], deleteUsers);

module.exports = router;