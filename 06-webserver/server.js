const express = require('express');
const morgan = require('morgan');
const path = require('path');
const app = express();

const hbs  = require('hbs');
// helpers
require('./hbs/helpers');

const PORT = process.env.PORT || 3000

app.use(morgan('dev'));
app.use( express.static(path.join(__dirname, 'public')));

//Express hbs
hbs.registerPartials( path.join(__dirname, 'views', 'partials') );
app.set('view engine','hbs');

app.get('/', (req, res) => {

    res.render('home.hbs', {
        name: 'johan'
    });

});

app.get('/about', (req, res) => {

    res.render('about.hbs');

});

app.listen(PORT, () => {
    console.log(`Server on port ${PORT}`)
});