const fs = require('fs');

let listTODO = [];

const saveDB = () => {
    let data = JSON.stringify(listTODO);

    fs.writeFile('db/data.json', data, (err) => {
        if (err) throw new Error('data no save', err);
    });
}

const loadDB = () => {
    try {
        listTODO = require('../db/data.json');
    } catch (error) {
        listTODO = [];
    }
}

const getListado = () => {
    loadDB();
    return listTODO;
}

const actualizar = (description, completado = true) => {
    loadDB();

    let index = listTODO.findIndex(task => {
        return task.description === description;
    });

    if(index >= 0) {
        listTODO[index].completado = completado;
        saveDB();
        return true;
    } else {
        return false;
    }
}

const crear = (description) => {
    
    loadDB();

    let TODO = {
        description,
        completado: false
    };

    listTODO.push(TODO);
    saveDB();
    
    return TODO;
}

const deleted = (description) => {
    loadDB();

    let newList = listTODO.filter(desc => {
       return desc.description !== description
    });

    if (newList.length === listTODO.length) {
        return false;
    } else {
        listTODO = newList;
        saveDB()
        return true;
    }
}

module.exports = {
    crear,
    getListado,
    actualizar,
    deleted
}