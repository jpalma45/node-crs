const { Router } = require('express');
const app = Router();

const { saveProduct, getProduct, getProducts, 
        updateProduct, deleteProduct, getPerTermQ } = require('../controller/product.controller');
const { validateToke } = require('../middleware/authentication');


app.post('/product', validateToke, saveProduct);
app.get('/products', validateToke, getProducts);
app.get('/product/:id', validateToke, getProduct);
app.get('/product/search/:term', validateToke, getPerTermQ);
app.put('/product/:id', validateToke, updateProduct);
app.delete('/product/:id', validateToke, deleteProduct);

module.exports = app;