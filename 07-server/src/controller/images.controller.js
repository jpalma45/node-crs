const fs = require('fs'); 
const path = require('path');

const viewImage = ( req, res, next ) => {
    let { type, img } = req.params;

    let pathImage = path.join(__dirname, `../uploads/${ type }/${ img }`); 

    if (fs.existsSync(pathImage)) {
        res.sendFile(pathImage);
    } else {
        let noImagePath = path.join(__dirname, '../assets/no-image.jpg');
        res.sendFile(noImagePath);
        
    }

}

module.exports = {
    viewImage
};