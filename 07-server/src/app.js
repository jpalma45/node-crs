const express = require('express');
const morgan = require('morgan');
const app = express(); 
const path = require('path');
const expressUploadFile = require('express-fileupload');

// middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(express.static( path.join(__dirname, 'public') ));
app.use(expressUploadFile({ useTempFiles: true }));

// Use routes index
app.use('/api', require('./router/index'))


module.exports = app;