const User = require('../models/user.model');
const Product = require('../models/products.model');
const path = require('path');
const fs = require('fs');

const uploadImage = (req, res, next) => {

    const { type, id } = req.params;

    if(!req.files){
        return res.status(400)
                    .json({
                        ok: false,
                        err: {
                            message: 'No file were uploaded'
                        }
                    });
    }

    // validate type
    const ValidType = ['product', 'user'];

    if (ValidType.indexOf(type) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'type is no valid the type valid are: ' + ValidType.join(', '),
                ext: type
            }
        });
    }

    let { FileUpload } = req.files;
    let FileNameCut = FileUpload.name.split('.');
    let extension = FileNameCut[FileNameCut.length -1];

    // valid extensions
    let ValidExtensions = ['png', 'jpg', 'gif', 'jpeg'];

    if( ValidExtensions.indexOf(extension) < 0 ){
        return res.status(400).json({
            ok: false,
            err: {
                message: 'extension is no valid the extension valid are: ' + ValidExtensions.join(', '),
                ext: extension
            }
        });
    }

    // Change name of the file
    let fileNameUpdated = `${ id }-${ new Date().getMilliseconds() }.${ extension }`

    FileUpload.mv(path.join(__dirname, '../uploads', `${type}`, `${ fileNameUpdated }`), (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        // hare, upload user image to the DB
        if (type === 'user') {
            userImage(id, fileNameUpdated, res);
        } else {
            productImage(id, fileNameUpdated, res)
        }
    });
}

const userImage  = (id,  fileName, res) => {
    User.findById(id, (err, userDB) =>{
        if ( err ) {
            // remove img when update user img
            removeFile(fileName, 'user');

            return res.status(500).json({
                ok: false,
                err
            });
        }

        if ( !userDB ) {
            // remove img when update user img
            removeFile(fileName, 'user');

            return res.status(400).json({
                ok: false,
                err: {
                    message: 'user don no exist'
                }
            }); 
        }

        // remove img when update user img
        removeFile(userDB.img, 'user');

        userDB.img = fileName;

        userDB.save((err, userSave) => {
            res.json({
                ok: true,
                user: userSave,
                img: fileName
            });
        });
    });
}

const productImage = (id,  fileName, res) => {
    Product.findById(id, (err, productsDB) =>{
        if ( err ) {
            // remove img when update Products img
            removeFile(fileName, 'product');

            return res.status(500).json({
                ok: false,
                err
            });
        }

        if ( !productsDB ) {
            // remove img when update Products img
            removeFile(fileName, 'product');

            return res.status(400).json({
                ok: false,
                err: {
                    message: 'product don no exist'
                }
            }); 
        }

        // remove img when update product img
        removeFile(productsDB.img, 'product');

        productsDB.img = fileName;

        productsDB.save((err, productSave) => {
            res.json({
                ok: true,
                product: productSave,
                img: fileName
            });
        });
    });
}

const removeFile = (nameImage, type) => {
    let pathImage = path.join(__dirname, `../uploads/${ type }/${ nameImage }`); 
    if ( fs.existsSync(pathImage) ) {
        fs.unlinkSync(pathImage);
    }
}

module.exports = {
    uploadImage
}