const { Router } = require('express');
const { viewImage } = require('../controller/images.controller');

const app = Router();

const { validateTokenImg } = require('../middleware/authentication');

app.get('/image/:type/:img', [ validateTokenImg ], viewImage)

module.exports = app;